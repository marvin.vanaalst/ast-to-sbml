# type: ignore

import math
import pytest
from ast_to_sbml import stringify
from typing import Callable


def test_simple_function():
    def rate(x, y, k_fwd):
        return x * y * k_fwd

    assert stringify(rate) == "x*y*k_fwd"


def test_simple_function_with_docstring():
    def rate(x, y, k_fwd):
        """Docstring"""
        return x * y * k_fwd

    assert stringify(rate) == "x*y*k_fwd"


def test_assignment():
    def rate(x, y, k_fwd):
        z = x * y
        return z * k_fwd

    assert stringify(rate) == "x*y*k_fwd"


def test_function_call_1_arg():
    def func(x):
        return x

    def rate(x, k_fwd):
        return func(x) * k_fwd

    assert stringify(rate) == "func(x)*k_fwd"


def test_function_call_2_args():
    def func(x, y):
        return x * y

    def rate(x, y, k_fwd):
        return func(x, y) * k_fwd

    assert stringify(rate) == "func(x,y)*k_fwd"


def test_function_call_with_binop():
    def rate(x, y, k_fwd):
        return math.ceil(x / y) * k_fwd

    assert stringify(rate) == "ceil(x/y)*k_fwd"


def test_function_call_with_assignment():
    def rate(x, y, k_fwd):
        a = x / y
        return math.ceil(a) * k_fwd

    assert stringify(rate) == "ceil(x/y)*k_fwd"
