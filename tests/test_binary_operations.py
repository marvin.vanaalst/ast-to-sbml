# type: ignore

import pytest
from ast_to_sbml import stringify


def test_add():
    def rate(x, y):
        return x + y

    assert stringify(rate) == "x+y"


def test_subtract():
    def rate(x, y):
        return x - y

    assert stringify(rate) == "x-y"


def test_multiply():
    def rate(x, y):
        return x * y

    assert stringify(rate) == "x*y"


def test_divide():
    def rate(x, y):
        return x / y

    assert stringify(rate) == "x/y"


def test_power():
    def rate(x, y):
        return x**y

    assert stringify(rate) == "x^y"


def test_floor():
    def rate(x, y):
        return x // y

    pytest.raises(NotImplementedError)


def test_modulus():
    def rate(x, y):
        return x % y

    pytest.raises(NotImplementedError)
