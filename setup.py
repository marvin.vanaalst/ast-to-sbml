# type: ignore
import codecs
import os
import pathlib
import re
from setuptools import find_packages, setup

here = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    with codecs.open(os.path.join(here, *parts), "r") as fp:
        return fp.read()


def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")


setup(
    name="ast_to_sbml",
    version=find_version("ast_to_sbml", "__init__.py"),
    description="Parse python functions into sbml",
    author="Marvin van Aalst",
    author_email="marvin.van.aalst@hhu.de",
    maintainer_email="marvin.van.aalst@hhu.de",
    license="GPL3",
    packages=find_packages("."),
    platforms="any",
    package_data={"src": ["py.typed"]},
    python_requires=">=3.7.0",
    zip_safe=False,
)
