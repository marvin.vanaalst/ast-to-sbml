import ast
from .data import AstFunction, Attribute, BinOp, Call, Node, SingleAssignment
from functools import singledispatch
from typing import Any, List, cast


def _strip_docstring(body: List[ast.stmt]) -> List[ast.stmt]:
    first = body[0]
    if isinstance(first, ast.Expr) and isinstance(first.value, ast.Str):
        return body[1:]
    return body


@singledispatch
def handle_node(node: Node) -> Any:
    """Handle an ast node.
    This function is overloaded for all the respective node classes.
    """
    raise NotImplementedError(f"Unchecked node type {node.__class__}")


@handle_node.register
def _handle_name(node: ast.Name) -> str:
    return node.id


@handle_node.register
def _handle_constant(node: ast.Constant) -> str:
    return str(node.value)


@handle_node.register
def _handle_return(body: ast.Return) -> Node:
    return cast(Node, handle_node(body.value))


@handle_node.register
def _handle_function(node: ast.FunctionDef) -> AstFunction:
    return AstFunction(
        name=node.name,
        args=[i.arg for i in node.args.args],
        children=[handle_node(i) for i in _strip_docstring(node.body)],
    )


@handle_node.register
def _handle_attribute(node: ast.Attribute) -> Attribute:
    return Attribute(
        name=node.attr,
        value=handle_node(node.value),
    )


@handle_node.register
def _handle_binop(node: ast.BinOp) -> BinOp:
    return BinOp(
        name=node.op.__class__.__name__,
        children=[handle_node(node.left), handle_node(node.right)],
    )


@handle_node.register
def _handle_single_assign(node: ast.Assign) -> SingleAssignment:
    if len(node.targets) > 1:
        raise ValueError("SBML does not support tuple unpacking")
    return SingleAssignment(
        target=handle_node(node.targets[0]),
        value=handle_node(node.value),
    )


@handle_node.register
def _handle_call(node: ast.Call) -> Call:
    return Call(
        name=handle_node(node.func),
        children=[handle_node(i) for i in node.args],
    )


###############################################################################
# To implement
###############################################################################


@handle_node.register
def _handle_if(node: ast.If) -> None:
    raise NotImplementedError("Apparently the dev was lazy")


@handle_node.register
def _handle_boolop(node: ast.BoolOp) -> None:
    raise NotImplementedError("Apparently the dev was lazy")


@handle_node.register
def _handle_unaryop(node: ast.UnaryOp) -> None:
    raise NotImplementedError("Apparently the dev was lazy")


###############################################################################
# Not supported by sbml
###############################################################################


@handle_node.register
def _handle_augmented_assign(node: ast.AugAssign) -> None:
    raise ValueError("SBML does not support augmented assignments")


@handle_node.register
def _handle_lambda(node: ast.Lambda) -> None:
    raise ValueError("SBML does not support lambda expressions")


@handle_node.register
def _handle_for(node: ast.For) -> None:
    raise ValueError("SBML does not support loops")


@handle_node.register
def _handle_while(node: ast.While) -> None:
    raise ValueError("SBML does not support loops")


@handle_node.register
def _handle_with(node: ast.With) -> None:
    raise ValueError("SBML does not support with statements")


@handle_node.register
def _handle_raise(node: ast.Raise) -> None:
    raise ValueError("SBML does not support raise statements")


@handle_node.register
def _handle_try(node: ast.Try) -> None:
    raise ValueError("SBML does not support try statements")


@handle_node.register
def _handle_assert(node: ast.Assert) -> None:
    raise ValueError("SBML does not support assert statements")


@handle_node.register
def _handle_global(node: ast.Global) -> None:
    raise ValueError("SBML does not support global variable calls")


@handle_node.register
def _handle_nonlocal(node: ast.Nonlocal) -> None:
    raise ValueError("SBML does not support nonlocal statements")


@handle_node.register
def _handle_pass(node: ast.Pass) -> None:
    raise ValueError("SBML does not support pass statements")


@handle_node.register
def _handle_break(node: ast.Break) -> None:
    raise ValueError("SBML does not support break statements")


@handle_node.register
def _handle_continue(node: ast.Continue) -> None:
    raise ValueError("SBML does not support continue statements")


@handle_node.register
def _handle_tuple(node: ast.Tuple) -> None:
    raise ValueError("SBML does not support tuples")


def parse_ast_function(module: ast.Module) -> AstFunction:
    body = cast(ast.FunctionDef, module.body[0])
    return cast(AstFunction, handle_node(body))
