from .data import ContainerNode, SingleAssignment
from copy import deepcopy
from typing import List


def collect_assignments(
    node: ContainerNode, assignments: List[SingleAssignment]
) -> List[SingleAssignment]:
    if hasattr(node, "children"):
        for child in node.children:
            if isinstance(child, SingleAssignment):
                assignments.append(child)
            elif isinstance(child, ContainerNode):
                collect_assignments(child, assignments)
    return assignments


def convert_assignments(
    node: ContainerNode, assignments: List[SingleAssignment]
) -> ContainerNode:
    node = deepcopy(node)
    if hasattr(node, "children"):
        new_children = []
        for child in node.children:
            if isinstance(child, ContainerNode):
                child = convert_assignments(child, assignments)
            elif isinstance(child, str):
                for assignment in assignments:
                    if child == assignment.target:
                        child = assignment.value
                        break
            if not isinstance(child, SingleAssignment):
                new_children.append(child)
        node.children = new_children
    return node
