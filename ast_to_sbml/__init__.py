__version__ = "0.1.0"

import ast
import inspect
import re
from .fixing import collect_assignments, convert_assignments
from .parsing import parse_ast_function
from typing import Callable


def get_ast_module(function: Callable) -> ast.Module:
    src = inspect.getsource(function)
    try:
        module = ast.parse(src)
    except IndentationError:
        initial_whitespace = re.match(r"^\s+", src)
        if initial_whitespace is not None:
            amt_whitespace = len(initial_whitespace.group())
            src = "\n".join(line[amt_whitespace:] for line in src.splitlines())
        module = ast.parse(src)
    return module


def stringify(fn: Callable) -> str:
    module = get_ast_module(fn)
    node = parse_ast_function(module)
    assignments = collect_assignments(node, [])
    return str(convert_assignments(node, assignments))
