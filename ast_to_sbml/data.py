from dataclasses import dataclass
from typing import List, Union


@dataclass
class Node:
    pass


@dataclass
class ContainerNode(Node):
    children: List[Union[Node, "ContainerNode"]]


@dataclass
class AstFunction(ContainerNode):
    name: str
    args: List[str]

    def __str__(self) -> str:
        return "".join(str(i) for i in self.children)


@dataclass
class BinOp(ContainerNode):
    name: str

    def __post_init__(self) -> None:
        self.operators = {
            "Add": "+",
            "Sub": "-",
            "Mult": "*",
            "Div": "/",
            "Pow": "^",
        }

    def __str__(self) -> str:
        op = self.operators[self.name]
        return op.join(str(i) for i in self.children)


@dataclass
class SingleAssignment(Node):
    target: str
    value: Node


@dataclass
class Attribute(Node):
    name: str
    value: str


@dataclass
class Call(ContainerNode):
    name: Union[Attribute, str]

    def __str__(self) -> str:
        if isinstance(self.name, Attribute):
            fn_name = self.name.name
        else:
            fn_name = self.name
        args = ",".join(str(i) for i in self.children)
        return f"{fn_name}({args})"
